Source: teamsploit
Section: net
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Raphaël Hertzog <buxy@kali.org>,
           Sophie Brun <sophie@offensive-security.com>,
           Ben Wilson <g0tmi1k@kali.org>,
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Homepage: http://www.teamsploit.com
Vcs-Git: https://gitlab.com/kalilinux/packages/teamsploit.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/teamsploit

Package: teamsploit
Architecture: all
Depends: ${misc:Depends},
         gnome-terminal,
         metasploit-framework,
         ruby:any,
         ${ruby:Depends}
Suggests: ruby-vte,
          ruby-gtk2,
          ruby-gtk-webkit,
          ruby-ponder,
          ruby-eventmachine
Description: Tools for group based penetration testing
 TeamSploit makes group-based penetration testing fun and easy, providing
 real-time collaboration and automation. TeamSploit is a suite of tools for
 the Metasploit Framework. TeamSploit should work with any MSF product
 (including OpenSource, Express, or Pro).
 .
 Features include:
  * Exploitation Automation
  * Automated Post-Exploitation
  * Information and Data Gathering
  * Session Sharing
  * Trojans and Trollware
 .
 TeamSploit's primary goal is to automate common penetration testing tasks, and
 provide access and information to fellow team members.
